import 'react-native';
import React from 'react';
import ButtonWithBackground from '../src/components/UI/ButtonWithBackground/ButtonWithBackground';
import {shallow,configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import renderer from 'react-test-renderer';


configure({ adapter: new Adapter() })


it('renders correctly', () => {
  const tree = renderer.create(
    <ButtonWithBackground />
    ).toJSON();
  expect(tree).toMatchSnapshot();
});



  describe('<ButtonWithBackground/>',()=>{
    it('renders 1 <ButtonWithBackground/> component',()=>{
      const component=shallow(<ButtonWithBackground/>);
      expect(component).toHaveLength(1);
    })
    });