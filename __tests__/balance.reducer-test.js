import "react-native";
import React from "react";
import balance from "../src/store/reducers/balance";
import { LOGIN_RESPONSE,TOKEN_RESPONSE,SHOW_PROGRESS,STOP_PROGRESS } from "../src/store/actions/actionTypes";



describe("balance reducer", () => {
  it("has a default state", () => {
    expect(balance(undefined,{
        type: "sample",
        result: null
      })).toEqual({
        isLoading:false,
      isLoggedIn: false,
      result: {},
      tokenResult: []
    });
  });

  it("can handle LOGIN_RESPONSE", () => {
    expect(balance(undefined,{
        type: LOGIN_RESPONSE,
        result: {"id":1}
      })).toEqual({
        isLoading:false,
        isLoggedIn: true,
        result: {"id":1},
        tokenResult: []
    });
  });

  it("can handle TOKEN_RESPONSE", () => {
    expect(balance(undefined,{
        type: TOKEN_RESPONSE,
        tokenResult: [1,2,3]
      })).toEqual({
        isLoading:false,
        isLoggedIn: false,
        result: {},
      tokenResult: [1,2,3]
    });
  });

  it("can handle SHOW_PROGRESS", () => {
    expect(balance(undefined,{
        type: SHOW_PROGRESS,
      })).toEqual({
        isLoading:true,
        isLoggedIn: false,
        result: {},
      tokenResult: []
    });
  });


  it("can handle STOP_PROGRESS", () => {
    expect(balance(undefined,{
        type: STOP_PROGRESS,
      })).toEqual({
        isLoading:false,
        isLoggedIn: false,
        result: {},
      tokenResult: []
    });
  });
});
