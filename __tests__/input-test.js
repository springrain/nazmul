import 'react-native';
import React from 'react';
import DefaultInput from '../src/components/UI/DefaultInput/DefaultInput';
import {shallow,configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import renderer from 'react-test-renderer';

configure({ adapter: new Adapter() })

it('renders correctly', () => {
  const tree = renderer.create(
    <DefaultInput />
    ).toJSON();
  expect(tree).toMatchSnapshot();
});

describe('<DefaultInput/>',()=>{
  it('renders 1 <DefaultInput/> component',()=>{
    const component=shallow(<DefaultInput/>);
    expect(component).toHaveLength(1);
  })
  });