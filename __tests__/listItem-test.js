import 'react-native';
import React from 'react';
import ListItem from '../src/components/ListItem/ListItem';
import {shallow,configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import renderer from 'react-test-renderer';
configure({ adapter: new Adapter() })

it('renders correctly', () => {
  const tree = renderer.create(
    <ListItem />
    ).toJSON();
  expect(tree).toMatchSnapshot();
});


describe('<ListItem/>',()=>{
  it('renders 1 <ListItem/> component',()=>{
    const component=shallow(<ListItem/>);
    expect(component).toHaveLength(1);
  })
  });