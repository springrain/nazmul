import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  KeyboardAvoidingView
} from "react-native";
import DefaultInput from "../../components/UI/DefaultInput/DefaultInput";
import validate from "../../utility/validation";
import ButtonWithBackground from "../../components/UI/ButtonWithBackground/ButtonWithBackground";
import { connect } from "react-redux";
import configureStore from "../../store/configureStore";
import { login, showProgress, stopProgress } from "../../store/actions/index";
import * as Progress from "react-native-progress";

class LoginScreen extends React.Component {
  state = {
    controls: {
      email: {
        value: "",
        valid: false,
        validationRules: {
          notEmpty: true
        }
      },
      password: {
        value: "",
        valid: false,
        validationRules: {
          notEmpty: true
        }
      }
    }
  };

  constructor(props) {
    super(props);
  }

  loginHandler = () => {
    const authData = {
      UserName: this.state.controls.email.value,
      Password: this.state.controls.password.value
    };
    if (this.state.controls.email.valid && this.state.controls.password.valid) {
      this.props.onLogin(authData);
    } else {
      alert("Username/Password is required");
    }
  };

  updateInputState = (key, value) => {
    let connectedValue = {};
    this.setState(prevState => {
      return {
        controls: {
          ...prevState.controls,
          [key]: {
            ...prevState.controls[key],
            value: value,
            valid: validate(
              value,
              prevState.controls[key].validationRules,
              connectedValue
            )
          }
        }
      };
    });
  };

  render() {
    let progress = <Progress.CircleSnail color={["red", "green", "blue"]} />;

    if (!this.props.isLoading) {
      progress = null;
    }

    return (
      <KeyboardAvoidingView style={styles.container}>
        <Image
          style={styles.logoStyle}
          source={require("../../assets/logo.png")}
        />
        <View style={styles.inputContainer}>
          <DefaultInput
            placeholder="Your Username"
            style={styles.input}
            value={this.state.controls.email.value}
            onChangeText={val => this.updateInputState("email", val)}
            valid={this.state.controls.email.valid}
            autoCapitalize="none"
            autoCorrect={false}
            keyboardType="email-address"
          />
          <DefaultInput
            placeholder="Password"
            style={styles.input}
            value={this.state.controls.password.value}
            onChangeText={val => this.updateInputState("password", val)}
            valid={this.state.controls.password.valid}
            secureTextEntry
          />
        </View>
        <ButtonWithBackground onPress={this.loginHandler}>
          Submit
        </ButtonWithBackground>
        {progress}
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
    width: "100%"
  },
  logoStyle: {
    width: 300,
    height: 300
  },
  input: {
    backgroundColor: "#eee",
    borderColor: "#bbb"
  },
  inputContainer: {
    width: "80%"
  }
});

const mapStateToProps = state => {
  return {
    isLoading: state.balance.isLoading
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onLogin: authData => dispatch(login(authData))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
