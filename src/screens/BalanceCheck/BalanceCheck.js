import React, { Component } from "react";
import { View, Text, StyleSheet, FlatList } from "react-native";
import { connect } from "react-redux";
import { Navigation } from "react-native-navigation";
import {
  getTokenList,
  showProgress,
  stopProgress
} from "../../store/actions/index";
import ListItem from "../../components/ListItem/ListItem";
import * as Progress from "react-native-progress";
import createStore from "../../store/configureStore";

const store = createStore();

class BalanceCheck extends Component {
  static navigatorButtons = {
    rightButtons: [
      {
        title: "Logout",
        id: "logout",
        showAsAction: "always",
        buttonColor: "orange",
        buttonFontSize: 14,
        buttonFontWeight: "600"
      }
    ]
  };
  
  constructor(props) {
    super(props);
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }

  onNavigatorEvent(event) {
    if (event.type == "NavBarButtonPress") {
      if (event.id == "logout") {
        Navigation.startSingleScreenApp({
          screen: {
            screen: "login",
            title: "Login",
            navigatorStyle: {
              navBarHidden: true
            }
          }
        });
      }
    }
  }

  componentDidMount() {
    const tokenData = {
      UserId: this.props.result.user_id
    };
    this.props.getToken(tokenData);
  }
  render() {
    let progress = (
      <Progress.CircleSnail
        color={["red", "green", "blue"]}
        size={90}
        style={styles.progressStyle}
      />
    );
    if (!this.props.isLoading) {
      progress = null;
    }
    return (
      <View>
        <View style={styles.headerText}>
          <Text style={styles.textHeading}>
            {this.props.result.first_name + " " + this.props.result.last_name}
          </Text>
        </View>
        {progress}
        <FlatList
          style={styles.listContainer}
          data={this.props.tokenResult}
          renderItem={info => (
            <ListItem
              token={info.item.token}
              currency={info.item.currency}
              tokenType={info.item.token_type}
              balance={info.item.available_balance}
            />
          )}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  headerText: {
    alignItems: "center",
    width: "100%",
    padding: 10,
    backgroundColor: "#fff"
  },
  listContainer: {
    width: "100%",
    marginRight: 20
  },
  textHeading: {
    fontSize: 28,
    fontWeight: "bold"
  },
  progressStyle: {
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    margin: 100
  }
});

const mapStateToProps = state => {
  return {
    isLoggedIn: state.balance.isLoggedIn,
    result: state.balance.result,
    tokenResult: state.balance.tokenResult,
    isLoading: state.balance.isLoading
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getToken: tokenData => dispatch(getTokenList(tokenData))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(BalanceCheck);
