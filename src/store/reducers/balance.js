import {
  LOGIN_RESPONSE,
  TOKEN_RESPONSE,
  SHOW_PROGRESS,
  STOP_PROGRESS
} from "../actions/actionTypes";

const initialState = {
  isLoading: false,
  isLoggedIn: false,
  result: {},
  tokenResult: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_RESPONSE:
      return {
        ...state,
        isLoggedIn: true,
        result: action.result
      };
    case TOKEN_RESPONSE:
      return {
        ...state,
        tokenResult: action.tokenResult
      };
    case SHOW_PROGRESS:
      return {
        ...state,
        isLoading: true
      };

    case STOP_PROGRESS:
      return {
        ...state,
        isLoading: false
      };
    default:
      return state;
  }
};

export default reducer;
