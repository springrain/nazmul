import { TOKEN_REQUEST, TOKEN_RESPONSE } from "./actionTypes";
import { Navigation } from "react-native-navigation";
import { showProgress, stopProgress } from "../actions/index";

export const getTokenList = tokenData => {
  return dispatch => {
    dispatch(showProgress());
    fetch("https://stage.aucorpws.com/webservice2.0/user/get/token/list", {
      method: "POST",
      body: JSON.stringify(tokenData),
      headers: {
        ApiUser: "ws2-mobile",
        ApiKey: "WJDtSxWPCZenceHrQvJFrE9AI5SDxXAwBnjAaFik9A0=",
        "Content-Type": "application/json"
      }
    })
      .catch(error => {
        console.log(error);
        dispatch(stopProgress());
      })
      .then(res => res.json())
      .then(parsedRes => {
        dispatch(stopProgress());
        if (parsedRes.status === 0) {
          const tokenList = [];
          for (let key in parsedRes.result) {
            tokenList.push({
              ...parsedRes.result[key],
              key: key
            });
          }
          dispatch(setTokenResponse(tokenList));
        }
        console.log(parsedRes);
      });
  };
};

export const setTokenResponse = tokenList => {
  console.log(tokenList);
  return {
    type: TOKEN_RESPONSE,
    tokenResult: tokenList
  };
};
