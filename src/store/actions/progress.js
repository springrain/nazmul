import {SHOW_PROGRESS,STOP_PROGRESS} from '../actions/actionTypes';


export const showProgress = () =>{
    return {
        type: SHOW_PROGRESS
    }
}

export const stopProgress = () =>{
    return {
        type: STOP_PROGRESS
    }
}