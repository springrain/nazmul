import { LOGIN_RESPONSE } from "./actionTypes";
import { Navigation } from "react-native-navigation";
import { showProgress, stopProgress } from "../actions/index";

export const login = authData => {
  return dispatch => {
    dispatch(showProgress());
    fetch("https://stage.aucorpws.com/webservice2.0/user/login", {
      method: "POST",
      body: JSON.stringify(authData),
      headers: {
        ApiUser: "ws2-mobile",
        ApiKey: "WJDtSxWPCZenceHrQvJFrE9AI5SDxXAwBnjAaFik9A0=",
        "Content-Type": "application/json"
      }
    })
      .catch(error => {
        console.log(error);
        dispatch(stopProgress());
      })
      .then(res => res.json())
      .then(parsedRes => {
        dispatch(stopProgress());
        if (parsedRes.status === 0) {
          dispatch(setLoginResponse(parsedRes.result));
          Navigation.startSingleScreenApp({
            screen: {
              screen: "balance",
              title: "My Balance"
            }
          });
        } else {
          alert("Username or password does not match");
        }
        console.log(parsedRes);
      });
  };
};

export const setLoginResponse = loginResponse => {
  console.log(loginResponse);
  return {
    type: LOGIN_RESPONSE,
    result: loginResponse
  };
};
