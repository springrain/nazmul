import React from "react";
import { View, Text, StyleSheet } from "react-native";

const listItem = props => (
  <View style={styles.listItem}>
    <Text style={styles.tokenStyle}>{"A/C No: " + props.token}</Text>
    <View style={styles.lineStyle} />
    <Text style={styles.textHeading}>{props.tokenType}</Text>
    <Text style={styles.balanceStyle}>
      {"Available Balance: " + props.currency + " " + props.balance}
    </Text>
  </View>
);

const styles = StyleSheet.create({
  listItem: {
    width: "100%",
    marginBottom: 20,
    padding: 10,
    alignItems: "center",
    backgroundColor: "#FF6E40",
    borderColor: "#fff",
    borderWidth: 10
  },
  tokenStyle: {
    fontSize: 18,
    color: "#fff"
  },
  lineStyle: {
    backgroundColor: "#ffff",
    width: "100%",
    height: 2,
    marginTop: 10,
    marginBottom: 10
  },
  textHeading: {
    fontSize: 24,
    fontWeight: "bold",
    color: "#fff"
  },
  balanceStyle: {
    fontSize: 14,
    color: "#fff"
  }
});

export default listItem;
