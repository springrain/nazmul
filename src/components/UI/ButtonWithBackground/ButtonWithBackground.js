import React from "react";
import {
  TouchableOpacity,
  Text,
  View,
  StyleSheet,
  Platform
} from "react-native";

const buttonWithBackground = props => {
  const content = (
    <View>
      <Text style={styles.buttonTextStyle}>{props.children}</Text>
    </View>
  );
  return (
    <TouchableOpacity onPress={props.onPress} style={styles.button}>
      {content}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    padding: 10,
    margin: 5,
    borderRadius: 5,
    alignItems: "center",
    backgroundColor: "#fe6335",
    width: "80%"
  },
  buttonTextStyle: {
    color: "#fff"
  }
});

export default buttonWithBackground;
