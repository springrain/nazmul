import { Navigation } from "react-native-navigation";
import { Provider } from "react-redux";
import LoginScreen from "./src/screens/Login/Login";
import BalanceCheckScreen from "./src/screens/BalanceCheck/BalanceCheck";
import configureStore from "./src/store/configureStore";

const store = configureStore();

Navigation.registerComponent("login", () => LoginScreen, store, Provider);

Navigation.registerComponent(
  "balance",
  () => BalanceCheckScreen,
  store,
  Provider
);

Navigation.startSingleScreenApp({
  screen: {
    screen: "login",
    title: "Login",
    navigatorStyle: {
      navBarHidden: true
    }
  }
});
