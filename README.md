## Table of Contents

* [Acceptence Criteria](#acceptence-criteria)
* [User Login Credential](#user-login-credential)
* [Installation instruction](#installation-instruction)

## Acceptence criteria

Acceptence criteria for reducers test code is listed as below :

1.  Reducer should have a default state.
2.  When LOGIN_RESPONSE type action is thrown , reducers state value should be updated accordingly with updated login api response result and isLoggedIn to true.
3.  When TOKEN_RESPONSE type action is thrown , reducers state value should be updated accordingly with updated tokenList api response result.
4.  When SHOW_PROGRESS type action is thrown , reducers state value should be updated accordingly with isLoading value to true.
5.  When STOP_PROGRESS type action is thrown , reducers state value should be updated accordingly with isLoading value to false.

Acceptence criteria for ButtonWithBackground component test code is listed as below :

1.  This component should rendered properly when it is called.
2.  When this component is rendered it should have 1 component.

Acceptence criteria for DefaultInput component test code is listed as below :

1.  This component should rendered properly when it is called.
2.  When this component is rendered it should have 1 component.

Acceptence criteria for ListItem component test code is listed as below :

1.  This component should rendered properly when it is called.
2.  When this component is rendered it should have 1 component.

## User Login Credential

UserName = fdewar27
Password = test123456

## Installation instruction

You will need Node,the React Native command line interface,Xcode,a JDK, and Android Studio.
I recommend installing Node using [Homebrew](https://brew.sh/). Run the following commands in a Terminal after installing Homebrew:

#### `brew install node`

If you have already installed Node on your system, make sure it is version 6 or newer.
Node comes with npm, which lets you install the React Native command line interface.Run the following command in a Terminal:

#### `npm install -g react-native-cli`

If Yarn was installed when the project was initialized, then dependencies will have been installed via Yarn, and you should probably use it to run these commands as well. Unlike dependency installation, command running syntax is identical for Yarn and NPM at the time of this writing.

#### `npm test`

Runs the [jest](https://github.com/facebook/jest) test runner on your tests.

#### `react-native run ios`

Installs your app in iOS emulator.

#### `react-native run android`

Installs your app in android emulator.
Make sure that your android studio emulation is running,before trying to install.
